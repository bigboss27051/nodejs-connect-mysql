const express = require('express')
const port = 3500;
const app = express();
const dbConnect = async (req, res) => {
    try {
        var knex = require('knex')({
            client: 'mysql',
            connection: {
                host: 'db',
                user: 'devuser',
                password: 'devpass',
                database: 'api'
            }
        });
        await knex.schema.createTable('users', function (table) {
            table.increments();
            table.string('name');
            table.timestamps();
        })
        res.send('ok')
    } catch (error) {
        res.send(error)
        console.log({ error });
    }

}
app.get('/test', dbConnect)

app.listen(port, function () {
    console.log(`Listening on port ${port}`)
})

